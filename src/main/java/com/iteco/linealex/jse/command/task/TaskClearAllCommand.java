package com.iteco.linealex.jse.command.task;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public final class TaskClearAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-clear-all";
    }

    @NotNull
    @Override
    public String description() {
        return "REMOVE ALL TASK EVERYWHERE";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User user = serviceLocator.getSelectedEntityService().getSelectedUser();
        @NotNull final Collection<Task> collection = serviceLocator.getTaskService().removeAllTasks(user.getId());
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY TASK]\n");
            return;
        }
        System.out.println("[ALL TASKS FROM ALL PROJECTS ARE REMOVED]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
