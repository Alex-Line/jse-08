package com.iteco.linealex.jse.command.project;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.exception.UserIsNotLogInException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectSelectCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-select";
    }

    @NotNull
    @Override
    public String description() {
        return "SELECT A PROJECT FOR MANIPULATION FURTHER";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User selectedUser = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (selectedUser == null) throw new UserIsNotLogInException();
        System.out.println("[ENTER THE NAME OF PROJECT FOR SELECTION]");
        @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
        @Nullable final Project selectedProject = serviceLocator.getProjectService().selectEntity(projectName, selectedUser.getId());
        if (selectedProject == null) {
            System.out.println("[THERE IS NOT SUCH PROJECT AS " + projectName
                    + ". PLEASE TRY TO SELECT AGAIN]\n");
            return;
        }
        serviceLocator.getSelectedEntityService().setSelectedProject(selectedProject);
        System.out.println("[WAS SELECTED]");
        System.out.println(selectedProject);
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
