package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserLogInCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "SIGN IN INTO YOUR ACCOUNT";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER USER LOGIN");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER USER PASSWORD");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        @Nullable final User user = serviceLocator.getUserService().signInUser(login, password,
                serviceLocator.getSelectedEntityService().getSelectedUser());
        if (user == null) System.out.println("[THERE WAS LOGIN OTHER USER. TRY AGAIN]\n");
        else System.out.println("[USER " + user.getName() + " ENTERED TO TASK MANAGER]\n");
        serviceLocator.getSelectedEntityService().setSelectedUser(user);
    }

    @Override
    public boolean secure() {
        return false;
    }

}
