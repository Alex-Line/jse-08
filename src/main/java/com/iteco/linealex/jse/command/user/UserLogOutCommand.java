package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public final class UserLogOutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "SIGN OUT OF YOUR ACCOUNT IF IT WAS SIGN IN";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getSelectedEntityService().getSelectedUser() == null) {
            System.out.println("[YOU HAVE NOT BEEN SIGN IN]\n");
            return;
        }
        serviceLocator.getSelectedEntityService().setSelectedUser(null);
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}
