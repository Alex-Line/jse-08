package com.iteco.linealex.jse.command.project;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "REMOVING A PROJECT BY NAME";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User user = serviceLocator.getSelectedEntityService().getSelectedUser();
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
        @Nullable Project project = null;
        if (user.getRole() == Role.ADMINISTRATOR) {
            project = serviceLocator.getProjectService().removeEntity(projectName);
        } else project = serviceLocator.getProjectService().removeEntity(projectName, user.getId());
        if (project == null) {
            System.out.println("[THERE IS NOT SUCH PROJECT! PLEASE TRY AGAIN!]\n");
            return;
        }
        serviceLocator.getSelectedEntityService().setSelectedProject(null);
        System.out.println("[REMOVING PROJECT]");
        @NotNull final Collection<Task> collection = serviceLocator.getTaskService().removeAllTasksFromProject(
                project.getId(), user.getId());
        if (!collection.isEmpty()) {
            System.out.println("[REMOVING TASKS FROM PROJECT]");
            System.out.println("[ALL TASKS REMOVED]");
        }
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
