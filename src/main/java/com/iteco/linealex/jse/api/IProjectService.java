package com.iteco.linealex.jse.api;

import org.jetbrains.annotations.*;

import java.util.Collection;

public interface IProjectService<Project> extends IService<Project> {

    @NotNull
    public Collection<Project> getAllEntities(@Nullable final String userId);

    @Nullable
    public Project removeEntity(
            @Nullable final String entityName,
            @Nullable final String userId);

    @Nullable
    public Project getEntityByName(
            @Nullable final String entityName,
            @Nullable final String userId);

    @NotNull
    public Collection<Project> removeAllEntities(@Nullable final String userId);

}
