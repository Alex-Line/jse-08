package com.iteco.linealex.jse.api;

import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;
import org.jetbrains.annotations.Nullable;

public interface ISelectedEntityService {

    @Nullable
    public Project getSelectedProject();

    @Nullable
    public Task getSelectedTask();

    @Nullable
    public User getSelectedUser();

    public void setSelectedProject(@Nullable final Project selectedProject);

    public void setSelectedTask(@Nullable final Task selectedTask);

    public void setSelectedUser(@Nullable final User selectedUser);

}
