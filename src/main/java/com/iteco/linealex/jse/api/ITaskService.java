package com.iteco.linealex.jse.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ITaskService<Task> extends IService<Task> {

    @NotNull
    Collection<Task> removeAllTasksFromProject(
            @Nullable final String projectName,
            @Nullable final String id);

    @Nullable
    public Task selectEntity(
            @Nullable final String projectId,
            @Nullable final String taskName,
            @Nullable final String userId);

    @Nullable
    public Task selectEntity(
            @Nullable final String taskName,
            @Nullable final String userId);

    @NotNull
    public Collection<Task> getAllEntities(@Nullable final String userId);

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String projectId,
            @Nullable final String userId);

    @Nullable
    public Task removeEntity(
            @Nullable final String entityName,
            @Nullable final String userId);

    @Nullable
    public Task removeEntity(
            @Nullable final String projectId,
            @Nullable final String entityName,
            @Nullable final String userId);

    @NotNull
    public Collection<Task> removeAllTasks(@Nullable final String userId);

    public boolean attachTaskToProject(
            @Nullable final String projectId,
            @Nullable final Task selectedEntity);

}
