package com.iteco.linealex.jse.api;

import com.iteco.linealex.jse.exception.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IUserService<User> extends IService<User> {

    @Nullable
    public User signInUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final User selectedUser
    ) throws Exception;

    @Nullable
    public User createUser(
            @Nullable final User user,
            @Nullable final User selectedUser
    ) throws TaskManagerException;

    @Nullable
    public User updateUserPassword(
            @Nullable final String oldPassword,
            @Nullable final String newPassword,
            @Nullable final User selectedUser
    ) throws Exception;

    @Nullable
    public User getUser(
            @Nullable final String login,
            @Nullable final User selectedUser
    ) throws TaskManagerException;

    @NotNull
    public Collection<User> getUsers(@Nullable final User selectedUser) throws LowAccessLevelException;

}
