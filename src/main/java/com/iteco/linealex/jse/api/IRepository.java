package com.iteco.linealex.jse.api;

import com.iteco.linealex.jse.exception.InsetExistingEntityException;
import org.jetbrains.annotations.*;

import java.security.NoSuchAlgorithmException;
import java.util.Collection;

public interface IRepository<T> {

    public boolean contains(
            @NotNull final String entityName);

    public boolean contains(
            @NotNull final String entityName,
            @NotNull final String userId);

    @NotNull
    public Collection<T> findAll();

    @Nullable
    public T findOne(@NotNull final String name);

    @Nullable
    public T persist(@NotNull final T example
    ) throws InsetExistingEntityException, NoSuchAlgorithmException;

    @Nullable
    public T merge(@NotNull final T example) throws InsetExistingEntityException;

    @Nullable
    public T remove(@NotNull final String name);

    @NotNull
    public Collection<T> removeAll();

}