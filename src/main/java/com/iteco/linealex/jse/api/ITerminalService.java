package com.iteco.linealex.jse.api;

import org.jetbrains.annotations.NotNull;

public interface ITerminalService {

    @NotNull
    public String nextLine();

}
