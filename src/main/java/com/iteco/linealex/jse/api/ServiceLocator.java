package com.iteco.linealex.jse.api;

import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;
import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {

    @NotNull
    IProjectService<Project> getProjectService();

    @NotNull
    ITaskService<Task> getTaskService();

    @NotNull
    IUserService<User> getUserService();

    @NotNull
    ITerminalService getTerminalService();

    @NotNull
    ISelectedEntityService getSelectedEntityService();

    @NotNull
    ICommandService getCommandService();

}
