package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.IProjectService;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.repository.AbstractRepository;
import com.iteco.linealex.jse.exception.InsetExistingEntityException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public final class ProjectService extends AbstractService<Project> implements IProjectService<Project> {

    public ProjectService(@NotNull final AbstractRepository<Project> repository) {
        super(repository);
    }

    @Nullable
    @Override
    public Project persist(@Nullable final Project project) throws InsetExistingEntityException {
        if (project == null) return null;
        if (project.getName() == null || project.getName().isEmpty()) return null;
        if (project.getDescription() == null || project.getDescription().isEmpty()) return null;
        if (project.getUserId() == null || project.getUserId().isEmpty()) return null;
        return repository.persist(project);
    }

    @Nullable
    @Override
    public Project merge(@Nullable final Project entity) {
        if (entity == null) return null;
        if (entity.getName() == null) return null;
        if (entity.getUserId() == null) return null;
        @Nullable final Project existedProject = repository.findOne(entity.getName(), entity.getUserId());
        if (existedProject == null) return null;
        existedProject.setDescription(entity.getDescription());
        existedProject.setDateStart(entity.getDateStart());
        existedProject.setDateFinish(entity.getDateFinish());
        return existedProject;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntities(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAll(userId);
    }

    @Nullable
    @Override
    public Project removeEntity(
            @Nullable final String projectName,
            @Nullable final String userId
    ) {
        if (projectName == null || projectName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @Nullable final Project project = repository.remove(projectName, userId);
        return project;
    }

    @NotNull
    @Override
    public Collection<Project> removeAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.removeAll(userId);
    }

    @Nullable
    @Override
    public Project getEntityByName(
            @Nullable final String projectName,
            @Nullable final String userId
    ) {
        if (projectName == null || projectName.isEmpty()) return null;
        if (userId == null) return null;
        return repository.findOne(projectName, userId);
    }

    @Nullable
    @Override
    public Project selectEntity(
            @Nullable final String entityName,
            @Nullable final String userId
    ) {
        if (entityName == null || entityName.isEmpty()) return null;
        return getEntityByName(entityName, userId);
    }

}