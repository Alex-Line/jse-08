package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.IService;
import com.iteco.linealex.jse.entity.AbstractEntity;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.repository.AbstractRepository;
import com.iteco.linealex.jse.exception.InsetExistingEntityException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;

@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @Nullable
    protected T selectedEntity;

    @NotNull
    protected AbstractRepository<T> repository;

    public AbstractService(@NotNull final AbstractRepository<T> repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public Collection<T> getAllEntities() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public abstract T persist(@Nullable final T entity) throws InsetExistingEntityException;

    @Nullable
    @Override
    public T removeEntity(@Nullable final String entityName) {
        if (entityName == null || entityName.isEmpty()) return null;
        @Nullable final T entity = repository.remove(entityName);
        if (entity != null) selectedEntity = null;
        return entity;
    }

    @Nullable
    public abstract T removeEntity(
            @Nullable final String entityName,
            @Nullable final String userId);

    @NotNull
    public Collection<Project> removeAllEntities(@Nullable final String userId) {
        return new ArrayList<>();
    }

}
