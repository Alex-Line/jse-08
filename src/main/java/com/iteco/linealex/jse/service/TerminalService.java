package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.ITerminalService;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public final class TerminalService implements ITerminalService {

    @NotNull
    private final Scanner scanner = new Scanner(System.in);

    @NotNull
    @Override
    public String nextLine() {
        return scanner.nextLine().trim();
    }

}
