package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.ISelectedEntityService;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;
import org.jetbrains.annotations.Nullable;

public final class SelectedEntityService implements ISelectedEntityService {

    @Nullable
    private Project selectedProject;

    @Nullable
    private Task selectedTask;

    @Nullable
    private User selectedUser;

    @Nullable
    @Override
    public Project getSelectedProject() {
        return selectedProject;
    }

    @Nullable
    @Override
    public Task getSelectedTask() {
        return selectedTask;
    }

    @Nullable
    @Override
    public User getSelectedUser() {
        return selectedUser;
    }

    @Override
    public void setSelectedProject(@Nullable final Project selectedProject) {
        this.selectedProject = selectedProject;
    }

    @Override
    public void setSelectedTask(@Nullable final Task selectedTask) {
        this.selectedTask = selectedTask;
    }

    @Override
    public void setSelectedUser(@Nullable final User selectedUser) {
        this.selectedUser = selectedUser;
    }

}
