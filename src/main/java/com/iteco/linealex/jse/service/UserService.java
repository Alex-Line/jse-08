package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.IUserService;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.repository.AbstractRepository;
import com.iteco.linealex.jse.util.*;
import com.iteco.linealex.jse.exception.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

public final class UserService extends AbstractService<User> implements IUserService<User> {

    public UserService(@NotNull final AbstractRepository<User> repository) {
        super(repository);
    }

    @Nullable
    public User signInUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser != null) return null;
        if (login == null || login.isEmpty()) throw new LoginIncorrectException();
        if (!repository.contains(login)) throw new UserIsNotExistException();
        if (password == null || password.isEmpty()) throw new WrongPasswordException();
        @NotNull final String hashPassword = TransformatorToHashMD5.getHash(password);
        @Nullable final User user = repository.findOne(login);
        if (user != null && user.getHashPassword() != null
                && !user.getHashPassword().equals(hashPassword)) throw new WrongPasswordException();
        return user;
    }

    @Nullable
    @Override
    public User createUser(
            @Nullable final User user,
            @Nullable final User selectedUser
    ) throws TaskManagerException {
        if (selectedUser == null) return null;
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        if (user == null) return null;
        if (user.getName() == null) return null;
        if (repository.contains(user.getName())) throw new InsetExistingEntityException();
        return persist(user);
    }

    @Nullable
    @Override
    public User selectEntity(
            @Nullable final String entityName,
            @Nullable final String userId
    ) {
        if (entityName == null || entityName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.findOne(entityName, userId);
    }

    @Nullable
    @Override
    public User persist(@Nullable final User entity) throws InsetExistingEntityException {
        if (entity == null) return null;
        if (entity.getName() == null || entity.getName().isEmpty()) return null;
        return repository.persist(entity);
    }

    @Nullable
    @Override
    public User merge(@Nullable final User entity) {
        if (entity == null) return null;
        if (entity.getName() == null) return null;
        @Nullable final User user = repository.findOne(entity.getName());
        if (user == null) return null;
        user.setName(entity.getName());
        user.setRole(entity.getRole());
        user.setHashPassword(entity.getHashPassword());
        return user;
    }

    @Nullable
    @Override
    public User removeEntity(
            @Nullable final String entityName,
            @Nullable final String userId
    ) {
        if (entityName == null || entityName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.remove(entityName, userId);
    }

    @Nullable
    public User updateUserPassword(
            @Nullable final String oldPassword,
            @Nullable final String newPassword,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) throw new UserIsNotLogInException();
        if (oldPassword == null || oldPassword.isEmpty()) throw new WrongPasswordException();
        @NotNull final String hashOldPassword = TransformatorToHashMD5.getHash(oldPassword);
        if (selectedUser.getHashPassword() != null
                && !selectedUser.getHashPassword().equals(hashOldPassword)) throw new WrongPasswordException();
        if (newPassword == null || newPassword.isEmpty()) throw new WrongPasswordException();
        if (newPassword.length() < 8) throw new ShortPasswordException();
        @NotNull final String hashNewPassword = TransformatorToHashMD5.getHash(newPassword);
        selectedUser.setHashPassword(hashNewPassword);
        return selectedUser;
    }

    @Nullable
    public User getUser(
            @Nullable final String login,
            @Nullable final User selectedUser
    ) throws TaskManagerException {
        if (selectedUser == null) throw new UserIsNotLogInException();
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        if (login == null || login.isEmpty()) throw new LoginIncorrectException();
        @Nullable final User user = repository.findOne(login);
        if (user == null) throw new UserIsNotExistException();
        return user;
    }

    @NotNull
    public Collection<User> getUsers(
            @Nullable final User selectedUser
    ) throws LowAccessLevelException {
        if (selectedUser == null) return Collections.EMPTY_LIST;
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        return repository.findAll();
    }

}
