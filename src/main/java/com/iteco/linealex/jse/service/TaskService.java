package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.ITaskService;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.repository.AbstractRepository;
import com.iteco.linealex.jse.exception.InsetExistingEntityException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

public final class TaskService extends AbstractService<Task> implements ITaskService<Task> {

    public TaskService(@NotNull final AbstractRepository<Task> repository) {
        super(repository);
    }

    @Nullable
    public Task selectEntity(
            @Nullable final String projectId,
            @Nullable final String taskName,
            @Nullable final String userId
    ) {
        if (projectId == null || projectId.isEmpty()) return selectEntity(taskName, userId);
        if (taskName == null || taskName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.findOne(taskName, projectId, userId);
    }

    @Nullable
    public Task selectEntity(
            @Nullable final String taskName,
            @Nullable final String userId
    ) {
        if (taskName == null || taskName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.findOne(taskName, userId);
    }

    @NotNull
    public Collection<Task> getAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAll(userId);
    }

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String projectId,
            @Nullable final String userId
    ) {
        if (projectId == null || projectId.isEmpty()) return getAllEntities(userId);
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAll(projectId, userId);
    }

    @Nullable
    @Override
    public Task persist(@Nullable final Task entity) throws InsetExistingEntityException {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return null;
        if (entity.getUserId() == null || entity.getUserId().isEmpty()) return null;
        return repository.persist(entity);
    }

    @Nullable
    @Override
    public Task merge(@Nullable final Task entity) throws InsetExistingEntityException {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return null;
        if (entity.getUserId() == null || entity.getUserId().isEmpty()) return null;
        return repository.merge(entity);
    }

    @Nullable
    public Task removeEntity(
            @Nullable final String entityName,
            @Nullable final String userId
    ) {
        if (entityName == null || entityName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.remove(entityName, userId);
    }

    @Nullable
    public Task removeEntity(
            @Nullable final String projectId,
            @Nullable final String entityName,
            @Nullable final String userId
    ) {
        if (projectId == null || projectId.isEmpty()) return removeEntity(entityName, userId);
        if (entityName == null || entityName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.remove(entityName, projectId, userId);
    }

    @NotNull
    public Collection<Task> removeAllTasksFromProject(
            @Nullable final String projectId,
            @Nullable final String userId
    ) {
        if (projectId == null || projectId.isEmpty()) return removeAllTasks(userId);
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.removeAll(userId, projectId);
    }

    @NotNull
    public Collection<Task> removeAllTasks(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.removeAll(userId);
    }

    public boolean attachTaskToProject(
            @Nullable final String projectId,
            @Nullable final Task selectedEntity
    ) {
        if (projectId == null || projectId.isEmpty()) return false;
        if (selectedEntity == null) return false;
        for (@NotNull final Task task : repository.findAll()) {
            if (task.getProjectId() != null) continue;
            if (!task.getId().equals(selectedEntity.getId())) continue;
            task.setProjectId(projectId);
            return true;
        }
        return false;
    }

}