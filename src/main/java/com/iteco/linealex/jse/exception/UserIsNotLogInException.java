package com.iteco.linealex.jse.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class UserIsNotLogInException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "THERE IS NOT ANY ACTIVE USER. PLEASE LOGIN AND TRY AGAIN. COMMAND WAS INTERRUPTED\n";
    }

}
