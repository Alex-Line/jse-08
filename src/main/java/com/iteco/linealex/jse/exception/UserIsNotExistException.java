package com.iteco.linealex.jse.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class UserIsNotExistException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "USER IS NOT EXIST. COMMAND WAS INTERRUPTED\n";
    }
}
