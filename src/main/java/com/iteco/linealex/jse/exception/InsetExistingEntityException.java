package com.iteco.linealex.jse.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class InsetExistingEntityException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "THERE IS SUCH ELEMENT ALREADY. COMMAND WAS INTERRUPTED\n";
    }

}
