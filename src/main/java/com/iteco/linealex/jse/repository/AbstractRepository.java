package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.IRepository;
import com.iteco.linealex.jse.entity.AbstractEntity;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.exception.InsetExistingEntityException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

@NoArgsConstructor
public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {


    @NotNull
    protected final Map<String, T> entityMap = new LinkedHashMap<>();

    public abstract boolean contains(
            @NotNull final String entityName,
            @NotNull final String userId);

    @NotNull
    @Override
    public Collection<T> findAll() {
        @NotNull final Collection<T> collection = new LinkedList<>(entityMap.values());
        return collection;
    }

    @NotNull
    public abstract Collection<T> findAll(
            @NotNull final String userId);

    @NotNull
    public Collection<T> findAll(
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        return Collections.EMPTY_LIST;
    }

    @Nullable
    @Override
    public T findOne(@NotNull final String entityName) {
        for (@NotNull final Map.Entry<String, T> entry : entityMap.entrySet()) {
            if (entry.getValue().getName().equals(entityName))
                return entry.getValue();
        }
        return null;
    }

    @Nullable
    public T findOne(
            @NotNull final String entityName,
            @NotNull final String userId
    ) {
        return null;
    }

    @Nullable
    public T findOne(
            @NotNull final String entityName,
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        return null;
    }

    @Nullable
    @Override
    public T persist(@NotNull final T example) throws InsetExistingEntityException {
        if (entityMap.containsValue(example)) throw new InsetExistingEntityException();
        return entityMap.put(example.getId(), example);
    }

    @Nullable
    @Override
    public T merge(@NotNull final T example) throws InsetExistingEntityException {
        if (entityMap.containsValue(example)) throw new InsetExistingEntityException();
        return entityMap.put(example.getId(), example);
    }

    @Nullable
    @Override
    public T remove(@NotNull final String entityName) {
        for (@NotNull final Map.Entry<String, T> entry : entityMap.entrySet()) {
            if (!entry.getValue().getName().equals(entityName)) continue;
            return entityMap.remove(entry.getKey());
        }
        return null;
    }

    @Nullable
    public T remove(
            @NotNull final String entityName,
            @NotNull final String userId
    ) {
        return null;
    }

    @Nullable
    public T remove(
            @NotNull final String name,
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        return null;
    }

    @NotNull
    @Override
    public Collection<T> removeAll() {
        final Collection<T> collection = new LinkedList<>(entityMap.values());
        entityMap.clear();
        return collection;
    }

    @NotNull
    public Collection<T> removeAll(@NotNull final String userId) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<Task> removeAll(
            @NotNull final String userId,
            @NotNull final String projectId) {
        return Collections.EMPTY_LIST;
    }

}
