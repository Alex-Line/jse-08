package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.IRepository;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.exception.InsetExistingEntityException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IRepository<Project> {

    @Override
    public boolean contains(@NotNull final String entityName) {
        return false;
    }

    public boolean contains(
            @NotNull final String name,
            @NotNull final String userId
    ) {
        for (@NotNull final Map.Entry<String, Project> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null) continue;
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (!entry.getValue().getName().equals(name)) continue;
            return true;
        }
        return false;
    }

    @NotNull
    @Override
    public Collection<Project> findAll(@NotNull final String userId) {
        @NotNull final Collection<Project> collection = new LinkedList<>();
        for (@NotNull final Map.Entry<String, Project> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null) continue;
            if (entry.getValue().getUserId().equals(userId))
                collection.add(entry.getValue());
        }
        return collection;
    }

    @Nullable
    @Override
    public Project findOne(
            @NotNull final String name,
            @NotNull final String userId
    ) {
        for (@NotNull final Map.Entry<String, Project> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null) continue;
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (entry.getValue().getName().equals(name)) {
                return entry.getValue();
            }
        }
        return null;
    }

    @NotNull
    @Override
    public Project persist(@NotNull final Project example) throws InsetExistingEntityException {
        for (@NotNull final Map.Entry<String, Project> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null) continue;
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getUserId().equals(example.getUserId())) continue;
            if (entry.getValue().getName().equals(example.getName()))
                throw new InsetExistingEntityException();
        }
        entityMap.put(example.getId(), example);
        return example;
    }

    @Nullable
    @Override
    public Project merge(@NotNull final Project example) {
        if (example.getUserId() == null) return null;
        if (example.getName() == null) return null;
        @Nullable final Project oldProject = findOne(example.getName(), example.getUserId());
        if (oldProject == null) return null;
        oldProject.setName(example.getName());
        oldProject.setDescription(example.getDescription());
        oldProject.setDateStart(example.getDateStart());
        oldProject.setDateFinish(example.getDateFinish());
        return oldProject;
    }

    @Nullable
    @Override
    public Project remove(
            @NotNull final String name,
            @NotNull final String userId
    ) {
        Project project = null;
        for (@NotNull final Map.Entry<String, Project> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null) continue;
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (entry.getValue().getName().equals(name)) {
                project = entry.getValue();
                entityMap.remove(project.getId());
            }
        }
        return project;
    }

    @NotNull
    @Override
    public Collection<Project> removeAll(@NotNull final String userId) {
        @NotNull final Collection<Project> collection = new ArrayList<>();
        for (@NotNull final Iterator<Map.Entry<String, Project>> iterator = entityMap.entrySet().iterator(); iterator.hasNext(); ) {
            @NotNull final Map.Entry<String, Project> entry = iterator.next();
            if (entry.getValue().getUserId() == null) continue;
            if (entry.getValue().getUserId().equals(userId)) {
                collection.add(entry.getValue());
                iterator.remove();
            }
        }
        return collection;
    }

}