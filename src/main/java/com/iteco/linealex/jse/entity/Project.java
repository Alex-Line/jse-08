package com.iteco.linealex.jse.entity;

import com.iteco.linealex.jse.util.DateFormatter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
@Setter
@Getter
public final class Project extends AbstractEntity {

    @NotNull
    private final String id = UUID.randomUUID().toString();

    @Nullable
    private String name = "unnamed project";

    @Nullable
    private String description = "";

    @Nullable
    private Date dateStart = new Date();

    @Nullable
    private Date dateFinish = new Date();

    @Nullable
    private String userId = null;

    @NotNull
    @Override
    public String toString() {
        return "Project: " + name +
                " { ID = " + id +
                ",\n    description = '" + description + '\'' +
                ",\n    Start Date = " + DateFormatter.formatDateToString(dateStart) +
                ",\n    Finish Date = " + DateFormatter.formatDateToString(dateFinish) +
                ",\n    User ID = " + userId +
                " }";
    }

}
