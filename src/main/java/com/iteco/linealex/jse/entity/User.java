package com.iteco.linealex.jse.entity;

import com.iteco.linealex.jse.enumerate.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

@NoArgsConstructor
@Setter
@Getter
public final class User extends AbstractEntity {

    @Nullable
    private String login;

    @Nullable
    private String hashPassword;

    @Nullable
    private Role role = Role.ORDINARY_USER;

    @NotNull
    private final String id = UUID.randomUUID().toString();

    public User(@NotNull final String login,
                @NotNull final String hashPassword) {
        this.login = login;
        this.hashPassword = hashPassword;
    }

    @NotNull
    @Override
    public String toString() {
        return "User " + login +
                ",\n     role = " + role.getName() +
                ",\n     id = " + id +
                '}';
    }
}
