package com.iteco.linealex.jse.entity;

import com.iteco.linealex.jse.exception.WrongDateFormatException;
import com.iteco.linealex.jse.util.DateFormatter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public final class Task extends AbstractEntity {

    @NotNull
    private final String id = UUID.randomUUID().toString();

    @Nullable
    private String name = "unnamed task";

    @Nullable
    private String description = "";

    @Nullable
    private Date dateStart = new Date();

    @Nullable
    private Date dateFinish = new Date();

    @Nullable
    private String projectId = null;

    @Nullable
    private String userId = null;

    @NotNull
    @Override
    public String toString() {
        return "Task " + name + " {" +
                "ID = " + id +
                ", \n    description='" + description + '\'' +
                ", \n    Start date = " + DateFormatter.formatDateToString(dateStart) +
                ", \n    Finish date = " + DateFormatter.formatDateToString(dateFinish) +
                ", \n    PROJECT_ID = " + projectId +
                ", \n    User ID = " + userId +
                '}';
    }

}
